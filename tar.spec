Name:          tar
Version:       1.30
Release:       10
Epoch:         2
Summary:       An organized and systematic method of controlling a large amount of data
License:       GPLv3+
URL:           http://www.gnu.org/software/tar/
Source0:       https://ftp.gnu.org/gnu/tar/tar-%{version}.tar.xz
Source1:       https://ftp.gnu.org/gnu/tar/tar-%{version}.tar.xz.sig

BuildRequires: autoconf automake texinfo gettext libacl-devel attr acl policycoreutils
Provides:      bundled(gnulib) /bin/tar /bin/gtar

#patch0001~0008 come from fedora
Patch0001:     tar-1.28-loneZeroWarning.patch
Patch0002:     tar-1.28-vfatTruncate.patch
Patch0003:     tar-1.29-wildcards.patch
Patch0004:     tar-1.28-atime-rofs.patch
Patch0005:     tar-1.28-document-exclude-mistakes.patch
Patch0006:     tar-1.28-sparse-inf-loops.patch
Patch0007:     tar-1.30-tests-difflink.patch
Patch0008:     tar-1.30-tests-dirrem.patch

Patch6000:     Rewrite-struct-tm-formatting.patch
Patch6001:     maint-avoid-warnings-from-upcoming-GCC8.patch
Patch6002:     maint-avoid-Wstringop-truncation-warnings-upcoming-G.patch
Patch6003:     maint-avoid-Wstringop-truncation-warnings-from-upcom.patch
Patch6004:     one-top-level-avoid-a-heap-buffer-overflow.patch
Patch6005:     Avoid-some-resource-leaks.patch
Patch6006:     Report-race-on-systems-without-O_DIRECTORY.patch
Patch6007:     Fix-double-free-introduced-by-577dc345.patch
Patch6008:     Fix-improper-memory-access.patch
Patch6009:     Fix-buffer-overflow.patch
Patch6010:     Reject-pax-options-starting-with-equals-sign.patch
Patch6011:     Fix-error-handling-when-reading-incremental-snapshot.patch
Patch6012:     Fix-semantics-of-K-used-together-with-explicit-membe.patch
Patch6013:     Bugfix.patch
Patch6014:     Disallow-modifications-to-the-global-pax-extended-he.patch
Patch6015:     Work-over-a-bug-in-gnulib-error.patch
Patch6016:     Remove-erroneous-abort-call.patch
Patch6017:     Fix-iconv-usage.patch
Patch6018:     Fix-compilation-without-iconv.patch
Patch6019:     Fix-build-on-AIX.patch
Patch6020:     Fix-possible-NULL-dereference-savannah-bug-55369.patch
Patch6021:     Fix-CVE-2018-20482.patch

%description
GNU Tar provides the ability to create tar archives, as well as various other
kinds of manipulation. For example, you can use Tar on previously created archives
to extract files, to store additional files, or to update or list files which were
already stored.

%package     help
Summary:     help document for tar
Requires:    %{name} = %{epoch}:%{version}-%{release}
BuildArch:   noarch

%description help
Help document for tar.

%prep
export FORCE_UNSAFE_CONFIGURE=1

%autosetup -n tar-%{version} -p1
autoreconf -v

%build
%configure --without-selinux --with-lzma="xz --format=lzma" \
           DEFAULT_RMT_DIR=%{_sysconfdir} RSH=/usr/bin/ssh
%make_build

%install
%make_install

ln -s tar      %{buildroot}%{_bindir}/gtar
ln -s tar.1.gz %{buildroot}%{_mandir}/man1/gtar.1

rm -f          %{buildroot}%{_infodir}/dir
rm -f          %{buildroot}%{_sysconfdir}/rmt
rm -f          %{buildroot}%{_mandir}/man8/rmt.8*

%find_lang tar


%check
#make check

%pre

%preun

%post

%postun

%files -f tar.lang
%doc AUTHORS README THANKS NEWS ChangeLog COPYING
%{_bindir}/*tar

%files help
%{_mandir}/man1/*tar.1*
%{_infodir}/tar.info*

%changelog
* Wed Jan 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:1.30-10
- set FORCE_UNSAFE_CONFIGURE to 1

* Wed Jan 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:1.30-9
- cancel make check

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:1.30-8
- Fix CVE-2018-20482

* Tue Sep 10 2019 Huiming Xie <xiehuiming@huawei.com> - 2:1.30-7
- Package init
